FROM meteormanaged/ember-deploy:test
RUN mkdir -p /usr/src/ember
WORKDIR /usr/src/ember

COPY / /usr/src/ember/

RUN npm install --silent 
RUN bower install --allow-root --silent 
RUN ember build --environment="production"

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]